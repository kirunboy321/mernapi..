
const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')
const multer = require('multer')
const path = require('path')

const app = express();
const authRoutes = require('./src/routes/auth')
const blogRoutes = require('./src/routes/blog')





const fileStorage = multer.diskStorage({
    destination : (req, file, cb) => {
        cb(null, 'images');
    },
    filename : (req, file, cb) => {
        cb(null, new Date().getTime() + '-' + file.originalname)
    }
})


const fileFilter = (req, file, cb) => {

    if( 
        file.mimetype === 'image/png' || 
        file.mimetype === 'image/jpg' ||
        file.mimetype === 'image/jpeg'
    ){
        cb(null, true);
    } else {
        // cb(null, false);
        cb({
            message : "format file harus jpg/jpeg/png"
        })
        
    }
}



app.use(multer({storage : fileStorage, fileFilter: fileFilter}).single('image'))
app.use('/images', express.static(path.join(__dirname + '/images')))
app.use(bodyParser.json()); //type JSON

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, DELETE, PATCH, PUT, OPTIONS');
    res.setHeader('Access-Contorl-Allow-Headers', 'Content-type, Authorization')
    next();
})



app.use('/v1/auth', authRoutes)
app.use('/v1/blog', blogRoutes)

app.use((error, req, res, next) => {
    const status = error.errorStatus || 500;
    const message = error.message;
    const data = error.data; 
    res.status(status).json({message: message, data : data});
})

const url = 'mongodb+srv://AHer:likujopo@cluster0.lmr0y.mongodb.net/blog?retryWrites=true&w=majority';
const connectionPrams = {
    useNewUrlParser: true,
    useCreateIndex : true,
    useUnifiedTopology : true,
}

mongoose.connect(url, connectionPrams)
.then(() => {

    app.listen(4000, () => console.log('Mongoose Connecting Success'));
}

)
.catch((err) => {
    console.log('error:', err)
})


