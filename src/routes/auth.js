const express = require('express')

const router = express.Router();

const authControllers = require('../controllers/auth')

//POST => v1/blog/post
router.use('/register', authControllers.register)

module.exports = router;

 
//test