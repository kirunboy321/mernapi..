const express = require('express')
const {body} =  require('express-validator')

const router = express.Router();

const blogControllers = require('../controllers/blog')

router.use('/post', 
[body('title').isLength({min : 5}).withMessage('input title tidak sesuai'), 
body('body').isLength({min : 5}).withMessage('input body tidak sesuai')],
 blogControllers.createBlogPost)

 router.get('/posts', blogControllers.getAllBlogPost)
 router.get('/postid/:postId', blogControllers.getBlogPostById)
 router.put('/postid/:postId',
 [body('title').isLength({min : 5}).withMessage('input title tidak sesuai'), 
body('body').isLength({min : 5}).withMessage('input body tidak sesuai')],
blogControllers.updateBlogPost)
router.delete('/postid/:postId', blogControllers.deleteBlogPost)

module.exports = router;
