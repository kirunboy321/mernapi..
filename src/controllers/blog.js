const {validationResult} = require('express-validator')
const path = require('path')
const fs = require('fs')
const BlogPost = require('../models/blog')


exports.createBlogPost = (req, res, next) => {
    const error = validationResult(req);

    if(!error.isEmpty()){
        const err = new Error('Invalid Value Tidak Sesuai');
        err.errorStatus = 400;
        err.data = error.array();
        throw err;
        
    }

    if(!req.file){
        const err = new Error('Image harus di upload')
        err.errorStatus = 422;
        throw err;
    }

    const title = req.body.title;
    const image = req.file.path;
    const body = req.body.body;



    const Posting = new BlogPost({
        title : title,
        body : body,
        image : image,
        author : {
            id : 1, 
            name : 'AHer'
        }
    })

    Posting.save()
    .then(result => {
        res.status(200).json({
            message : "Create Blog Post Success",
            data : result
        })
    })
    .catch(err => {
        console.log(err, 'error catch')
    })

   /* 
    data : result nya menggunakan variabel  bawah 

    const result = {
        message :"Create blog post success",
        data : {
            post_id : 1,
            title : title,
            image : "imagetest.png",
            body : body,
            created_at : "12/10/2021",
            author : {
                uid : 1,
                name : "testing"
            } 
        }
    }
    res.status(201).json(result) 
    */
}

exports.getAllBlogPost = (req, res, next) => {
    const currentPage = req.query.page || 1;
    const perPage = req.query.perPage || 5;
    let totalItems;

    BlogPost.find()
    .countDocuments()
    .then(count => {
        totalItems = count;
       return BlogPost.find()
        .skip((parseInt(currentPage) - 1) * parseInt(perPage))
        .limit(parseInt(perPage));
    })
    .then(result => {
        res.status(200).json({
            message : "Data Blog Berhasil Di Panggil",
            data : result,
            total_data : totalItems,
            per_page : perPage,
            current_page : currentPage,
        })
    })

    /*
    Ini untuk pencarian saja tanpa pagination, jadi ngerender semua data yang ratusan bahkan jutaan
    
    BlogPost.find()
    .then((result) => {
        res.status(200).json({
            message : "Data Blog Berhasil Di Panggil",
            data : result
        })
    })
    */
    .catch(err => {
        next(err);
    })
}


exports.getBlogPostById = (req, res, next) => {
    const postId = req.params.postId;
    BlogPost.findById(postId)
    .then(result => {
        if(!result) {

            // 2 Cara mengatasi error

            // return res.status(404).json({
            //     message : "Data anda tidak di temukan",
            //     data : result
            // })

            const error = new Error('Data Blog Post tidak di temukan')
            error.errorStatus = 404;
            throw error;

            /* 
            Noted Person :
            Masih belum berfungsi errornya  ketika ID nya tidak di temukan di DB
            "message": "Cast to ObjectId failed for value \"611911c5b812754476271870hhb\" (type string) at path \"_id\" for model \"BlogPost\"""
            erronya karena menambahkan jumlah idnya , hrusnya 15 di tmbah 18 . jadi errornya seprti diatas
            */


            
        }
        res.status(200).json({
            message : "Data Blog Post Berhasil Di Panggil",
            data : result
        })
    })
    .catch(err => {
        next(err);
        console.log(err, 'error  getBlogPostById')
    })
}

exports.updateBlogPost = (req, res, next) => {
    const error = validationResult(req);

    /*
        BAGAIMANA KETIKA UPDATE FOTO , FOTO YG LAMA IKUT TERHAPUS.
        KARENA UNTUK SEKARANG , KETIKA UPDATE FOTO. FOTO YANG LAMA TERTIMPA
    */

    if(!error.isEmpty()){
        const err = new Error('Invalid Value Tidak Sesuai');
        err.errorStatus = 400;
        err.data = error.array();
        throw err;
        
    }

    if(!req.file){
        const err = new Error('Image harus di upload')
        err.errorStatus = 422;
        throw err;
    }

    const title = req.body.title;
    const image = req.file.path;
    const body = req.body.body;
    const post = req.params.postId;

    BlogPost.findById(post)
    .then(post => {
        if(!post){
            const err = new Error('blog post update tidak di temukan');
            err.errorStatus = 404;
            throw err;
        }

        post.title = title;
        post.body = body;
        post.image = image;

        return post.save();

    })
    .then((result) => {
        res.status(200).json({
            message : 'Update Sukses',
            data : result
        })
    })
    .catch(err => {
        console.log(err, 'TESTINGG ERROR')
        next(err)
    })


   
}


exports.deleteBlogPost = (req, res, next) => {
    const postId = req.params.postId;

    BlogPost.findById(postId )
    .then(post => {
        if(!post){
            const error = new Erro('Blog Post Untuk Hapus Tidak Di Temukan')
            error.errorStatus = 404;
            throw error;
        }

        removeImage(post.image);
        console.log(post, "POST IMAGE")
      return BlogPost.findByIdAndRemove(postId)
    })
    .then(result => {
        res.status(200).json({
            message : 'Hapus Blog Post Berhasil',
            data : result
        })
    })
    .catch(err => {
        next(err)
    })
}

const removeImage = (filePath) => {
    console.log(filePath, 'FILEPATH IMAGE')
    console.log(__dirname, "dirname")
    // /Users/admin/Documents/cocolatos/mern-api/(../images/1629184292679-download.jpeg) 
    filePath = path.join(__dirname, '../..', filePath)
    fs.unlink(filePath, err => console.log(err, "unlink filepath") )
}